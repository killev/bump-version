[![pipeline status](https://gitlab.com/killev/bump-version/badges/master/pipeline.svg)](https://gitlab.com/killev/bump-version/commits/master)

[![coverage report](https://gitlab.com/killev/bump-version/badges/master/coverage.svg)](https://gitlab.com/killev/bump-version/commits/master)

**Version bump** is a lightweight web service which is designed to synchronize build bumpers and version across different branches, build machines etc.

Usage flow:

Service is designed to be used in the flat gitlab-flow process with one release and one hotfix being developed simultaneously. For example. Let's suppose you have just started development and you're working on version 1.0.0. So you have builds like 1.0.0 (1), 1.0.0 (20) etc. 

Once you release it you begin working on version 1.1.0. So you have builds like 1.1.0 (21), 1.1.0 (22) etc. 

Say, something should be fixed. So another branch of builds is created: 1.0.1 (23), 1.0.1(24). This branch exists until the next version is released.
It's really convenient when build number is 

Use cases
```
Current release 1.0.0 (build: 100)
    |-> Dev Hotfix (1.0.1)
    |-> Release Hotfix (1.0.1)
        |-> Current Release (1.0.1)
        |-> Dev Hotfix (1.0.2)
    |-> Dev next version (1.1.0)
    |-> Release version (1.1.0)
        |-> Current Release (1.1.0)
        |-> Dev Hotfix (1.1.1)
        |-> Dev next version (1.2.0)
```
Url: https://yb8h3houo9.execute-api.us-east-1.amazonaws.com/prod

Endpoints:
 - /version - responses current version of the API

# Technical Stack
* [Node.js](https://nodejs.org/en/)
* [VS Code](https://code.visualstudio.com/)
* [Jest](https://jestjs.io/)
* [Serverless.com](https://serverless.com/)
# Getting started
## Prerequisites:
* Add [ssh keys](https://docs.gitlab.com/ee/ssh/) to gitlab.
* Install docker
* Install VS Code with the following files Install the following addins:
    * [Coverage gutters] (https://marketplace.visualstudio.com/items?itemName=ryanluker.vscode-coverage-gutters)
    * [ESLint] (https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
    * [Node TDD] (https://marketplace.visualstudio.com/items?itemName=prashaantt.node-tdd)
* Install [Node.js](https://nodejs.org/en/)
## Beginning the work
* Pull dynamodb-local image:
```shell
docker pull amazon/dynamodb-local
```
* Clone git repository locally
```shell
git clone git@gitlab.com:killev/bump-version.git
cd bump-version
```
* Install dependencies:
```shell
npm install
```
* Run tests:
```shell
npm test
```
* Run service offline:
```shell
npm run build && npm run offline
```

# Task flow
* Before starting work on the task make sure its assigned to you
* Add the the "Doing" label to the task
* Create merge request
* Split task into small steps. 
* Run tests after every step. [Node TDD] (https://marketplace.visualstudio.com/items?itemName=prashaantt.node-tdd) shows you the status after every save.
* Commit every step.
* Commit message must contain explanation of what was done and tagword which shows the status of work:
    * Wip: #N  - task still in progress
    * Implemented: #N, Fixed: #N, Closes: #N - taks is finished.
    * Every commit without tagwork will be rejected. If mistake is made use **git commit --amend** command
* Check CI status after every commit

* When task is complemented:
    * Add one of the tagwords at the end of commit message: *Implemented: #N, Fixed: #N, Closes: #N*
    * Make sure that CI pipline should pass
    * Remove WIP status from merge request.
# List of commands:
* ```npm run build``` - builds the project
* ```npm run offline``` - runs built service offline using dev database
* ```npm run linter``` - validates code style using linter







