
const Bumper = require('../src/bump')
const Type = Bumper.Type
const bumper = new Bumper()

test('Increas minor by 1', () => {
    let current = { major: 1, minor: 1, hotfix: 1, build: 1}
    return bumper.next(current, Type.Minor )
        .then(res => {
            expect(res).toEqual( { major: 1, minor: 2, hotfix: 0, build: 2 } )
        })
})

test('Increases hotfix by 1', () =>
    bumper.next({major: 1, minor: 1, hotfix: 1, build: 1}, Type.Hotfix )
        .then(res => {
            expect(res).toEqual( { major: 1, minor: 1, hotfix: 2, build: 2 } )
        })
)


test('Wrong type should fail', () =>
    expect(bumper.next({major: 1, minor: 1, hotfix: 1, build: 1}, Type.Build ))
        .rejects.toThrow('Invalid build type (build). Only one of major, minor, hotfix are possible')
       
)