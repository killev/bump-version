'use strict'


const { ServerlessTestHelper, DynamoDBDocker } = require('serverless-test-helper')
const serverless = new ServerlessTestHelper('test')
const Storage = require('../src/storage')

const existingVersion = {
    bundleId: 'existing.bundle.id',
    version: {major: 1, minor: 1, hotfix: 1, build: 1}
}

const nonexistingVersion = {
    bundleId: 'nonexisting.bundle.id',
    version: {major: 0, minor: 0, hotfix: 0, build: 0}
}

const seeds = [
    {
        match: /version-bump-versions-(dev|test)/,
        items: () => [existingVersion]
    } 
]


describe('Storage test suite', ()=>{
    beforeAll(() => 
        Promise.all([DynamoDBDocker.launch(), serverless.init()]), 500000
    )

    afterAll(() =>
        DynamoDBDocker.finish()
    )

    beforeEach(() => 
        serverless.removeResources()
            .then(() => serverless.createResources(seeds))
            .catch(error =>{
                throw error
            }), 500000)

    afterEach(() => 
        serverless.removeResources()
    )

    test('version(:bundleId) should return current version for existing bundle', () => {
        let storage = new Storage()
        return storage.version(existingVersion.bundleId).then(version => {
            expect(version).toEqual(existingVersion)
        })
    }, 300000)

    test('version(:bundleId) should return defailt version for non-existing bundle', () => {
        let storage = new Storage()
        return storage.version(nonexistingVersion.bundleId).then(version => {
            expect(version).toEqual(nonexistingVersion)
        })
    }, 300000)

    test('version(:bundleId, :version) should update data with newVersion if provided', () => {
        let storage = new Storage()
        let newVersion = existingVersion
        newVersion.version.major = 2 
        return storage.version(newVersion.bundleId, newVersion.version).then(version => {
            expect(version).toEqual(newVersion)
        })
    }, 300000)
})