const { ServerlessTestHelper, DynamoDBDocker } = require('serverless-test-helper')
const serverless = new ServerlessTestHelper('test')

describe('version service integration tests', ()=> {

    beforeAll(() => 
        Promise.all([DynamoDBDocker.launch(), serverless.init()]), 500000
    )

    afterAll(() =>
        DynamoDBDocker.finish()
    )

    beforeEach(() =>
        serverless.removeResources()
            .then(() => serverless.createResources())
            .catch(error =>{
                throw error
            }), 500000)

    afterEach(() =>
        serverless.removeResources()
    )

    test('/version/', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })
        
        return wrapped.run({
            httpMethod: 'GET',
            path: '/version',
        }).then((response) => {
            const jsonObject = {
                bundle:'bump-version',
                version: '0.0.3'
            }
            expect(JSON.parse(response.body)).toMatchObject(jsonObject)
            expect(response.statusCode).toBe(200)
        })
    })

    test('/version/:bundleId should return current version', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })
        
        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/new-bundle',
        }).then((response) => {
            const jsonObject = {
                bundle:'new-bundle',
                version: { hotfix: 0, major: 0, minor: 0, build: 0 }
            }
            expect(JSON.parse(response.body)).toMatchObject(jsonObject)
            expect(response.statusCode).toBe(200)
        })
    })

    test('/version/:bundleId/next/:type/ should return next version', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })

        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/my-bundle/next/minor',
        }).then((response) => {
            const jsonObject = {
                bundle: 'my-bundle',
                version: { hotfix: 0, major: 0, minor: 1, build: 1 }
            }
            expect(JSON.parse(response.body)).toMatchObject(jsonObject)
            expect(response.statusCode).toBe(200)
        })
    })

    test('/version/:bundleId/next/:type/ should return save "build"', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })

        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/my-bundle-twice/next/minor',
        }).then(()=> wrapped.run({
            httpMethod: 'GET',
            path: '/version/my-bundle-twice/next/minor',
        })).then((response) => {
            const jsonObject = {
                bundle: 'my-bundle-twice',
                version: { hotfix: 0, major: 0, minor: 1, build: 2 }
            }
            expect(JSON.parse(response.body)).toMatchObject(jsonObject)
            expect(response.statusCode).toBe(200)
        })
    })

    test('/version/:bundleId/next/:type/ should return error if you pass wrong release type', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })

        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/my-bundle/next/minor1',
        }).then((response) => {
            const errorObj = {
                error: {
                    code: 100,
                    message: 'Invalid build type (minor1). Only one of major, minor, hotfix are possible'
                }
            }
            expect(JSON.parse(response.body)).toMatchObject(errorObj)
            expect(response.statusCode).toBe(400)
        })
    })

    test('/version/:bundleId/release/:type/ should return release version and save', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })

        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/my-bundle/release/hotfix',
        }).then((response) => {
            const jsonObject = {
                bundle: 'my-bundle',
                version: { hotfix: 1, major: 0, minor: 0, build: 1 }
            }
            expect(JSON.parse(response.body)).toMatchObject(jsonObject)
            expect(response.statusCode).toBe(200)
        })
    })

    test('/version/:bundleId/release/:type/ should return error when type is not appropriate', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })

        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/my-bundle/release/hotfix1',
        }).then((response) => {
            const errorObj = {
                error: {
                    code: 100,
                    message: 'Invalid build type (hotfix1). Only one of major, minor, hotfix are possible'
                }
            }
            expect(JSON.parse(response.body)).toMatchObject(errorObj)
            expect(response.statusCode).toBe(400)
        })
    })

    test('/version/:bundleId/:version should set release version', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })

        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/save-bundle/10.2.1 (10)',
        }).then((response) => {
            const jsonObject = {
                bundle: 'save-bundle',
                version: { major: 10, minor: 2, hotfix: 1, build: 10 }
            }
            expect(JSON.parse(response.body)).toMatchObject(jsonObject)
            expect(response.statusCode).toBe(200)
        })
    })

    test('/version/:bundleId/:version should return error when version is invalid', () => {
        const mod = require('../../src/index')
        const lambdaWrapper = require('lambda-wrapper')
        const wrapped = lambdaWrapper.wrap(mod, { handler: 'handler' })

        return wrapped.run({
            httpMethod: 'GET',
            path: '/version/save-bundle/10.2.1+(10',
        }).then((response) => {
            const errorObj = {
                error: {
                    message: 'Invalid version provided, the following format is accepted: 1.2.3 (5)'
                }
            }
            expect(JSON.parse(response.body)).toMatchObject(errorObj)
            expect(response.statusCode).toBe(400)
        })
    })
})
