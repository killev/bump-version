
// cases:
// minor
// - 1.0.0 (1), false -> 1.1.0 (1)
// - 1.0.2 (5), false -> 1.1.0 (6)
// - 1.1.0 (5), false -> 1.2.0 (6)
//major
// - 1.1.0 (5), false -> 2.0.0 (6)
// - 1.12.0 (5), false -> 2.0.0 (6)
//hotfix
// - 1.1.0 (5), false -> 2.0.0 (6)
// - 1.12.0 (5), false -> 2.0.0 (6)

const _ = require('lodash')
const Type = {
    Major: 'major',
    Minor: 'minor',
    Hotfix: 'hotfix',
    Build: 'build'
}
const order = [Type.Major, Type.Minor, Type.Hotfix]  

class Bumper {
    constructor(storage){
        this.storage = storage
    }
    next(current, type) {
    
        let index = order.findIndex(val=> val == type)
        
        if (index == -1) {
            let error = new Error(`Invalid build type (${type}). Only one of major, minor, hotfix are possible`)
            error.code = 100
            return Promise.reject(error)
        }
        let res = _.clone(current)
    
        res[order[index]] ++
        res[Type.Build] ++
        index++
        for (; index < order.length; index ++) {
            res[order[index]] = 0
        }
        return Promise.resolve(res)
    }
}
module.exports = Bumper
module.exports.Type = Type