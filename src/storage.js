
let AWS = require('aws-sdk')
function buildNewItem(tableName, bundleId, version) {
    if (version) {
        return { TableName: tableName,
            Item: {
                bundleId: bundleId,
                version: version
            }
        }
    }
    return { 
        TableName: tableName,
        ConditionExpression: 'attribute_not_exists(version)',
        Item: {
            bundleId: bundleId,
            version: {major: 0, minor: 0, hotfix: 0, build: 0}
        }
    }
}
class Storage {
    constructor() {
        this.document = new AWS.DynamoDB.DocumentClient()
        this.tableName = process.env.VERSIONS_TABLE
    }
    
    version(bundleId, version) {
        //return Promise.resolve({})
        const tableName = this.tableName
        
        let params = {
            TableName: tableName,
            Key: { bundleId: bundleId }
        }
        const document = this.document
        let newItem = buildNewItem(tableName, bundleId, version)
        return document.put(newItem).promise()
            .then(() => document.get(params).promise())
            .catch(() => document.get(params).promise())
            .then(item=>item.Item)
    }
}

module.exports = Storage