'use strict'

require('serverless-dynamodb-config').setup()

const serverless = require('serverless-http')
const express = require('express')
const packageJS = require('../package.json')
const app = express()

const Bumper = require('../src/bump')
const Storage = require('../src/storage')
const storage = new Storage()
const bumper = new Bumper()

app.set('json spaces', 2)
app.get('/version', function(req, res){
    res.json({
        bundle: packageJS.name,
        version: packageJS.version
    })
})
app.get('/version/:bundleId', function(req,res) {
    let bundle = req.params.bundleId
    storage.version(bundle).then(currentVersion => {
        res.json({
            bundle: bundle,
            version: currentVersion.version
        })
    })
})

app.get('/version/:bundleId/next/:type', function (req, res) {
    let bundle = req.params.bundleId
    let type = req.params.type
    storage.version(bundle).then(version => {
        let oldVersion = version.version
        return bumper.next(version.version, type)
            .then(nextVersion => ({ nextVersion, oldVersion }))
    }).then(versions => {
        let oldVersion = versions.oldVersion
        oldVersion.build = versions.nextVersion.build
        return storage.version(bundle, oldVersion).then(()=> versions.nextVersion )
    }).then(version=>{
        res.json({
            bundle: bundle,
            version: version
        })
    }).catch(error => {
        let errorObj = {
            error: {
                code: error.code,
                message: error.message
            }
        }
        res.status(400).json(errorObj)
    })
})

app.get('/version/:bundleId/release/:type', function (req, res) {
    let bundle = req.params.bundleId
    let type = req.params.type
    storage.version(bundle)
        .then(version => bumper.next(version.version, type))
        .then(nextVersion => storage.version(bundle, nextVersion))
        .then(version => {
            res.json({
                bundle: bundle,
                version: version.version
            })
        }).catch(error => {
            let errorObj = {
                error: {
                    code: error.code,
                    message: error.message
                }
            }
            res.status(400).json(errorObj)
        })
})

app.get('/version/:bundleId/:version', function (req, res) {
    let bundle = req.params.bundleId
    let version = req.params.version
    let regexp = /^(\d+)\.(\d+)\.(\d+)\s?\((\d+)\)$/
    
    if(regexp.test(version))  {
        let urlElems = regexp.exec(version).map(elem => parseInt(elem))
        let URLversion = { major: urlElems[1], minor: urlElems[2], hotfix: urlElems[3],  build: urlElems[4] }
        storage.version(bundle, URLversion).then(storageBundle => {
            res.send({
                bundle: bundle,
                version: storageBundle.version
            })
        })
    } else {
        let errorObj = {
            error: {
                message: 'Invalid version provided, the following format is accepted: 1.2.3 (5)'
            }
        }
        res.status(400).json(errorObj)
    }
})

module.exports.handler = serverless(app)