const path = require('path')
module.exports = {
    target: 'node',
    node: {
    // Need this when working with express, otherwise the build fails
        __dirname: false,   // if you don't put this is, __dirname
        __filename: false,  // and __filename return blank or /
    },
    entry: './src/index.js',
    output: {
        library: 'version-bump',
        libraryTarget: 'umd',
        umdNamedDefine: true,
        path: path.resolve(__dirname, 'dist'),
    }
}
    